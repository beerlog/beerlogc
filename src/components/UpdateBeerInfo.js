import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import axios from 'axios';
import '../App.css';

class UpdateBeerInfo extends Component {
  constructor(props) {
    super(props);
    this.state = {
      name: '',
      brewer:'',
      description:'',
      tasted_date:'',
      notes:''
    };
  }

  componentDidMount() {
    // console.log("Print id: " + this.props.match.params.id);
    axios
      .get('http://localhost:8082/beers/'+this.props.match.params.id)
      .then(res => {
        // this.setState({...this.state, beer: res.data})
        this.setState({
          name: res.data.name,
          brewer: res.data.brewer,
          description: res.data.description,
          tasted_date: res.data.tasted_date,
          notes: res.data.notes
        })
      })
      .catch(err => {
        console.log("Error from UpdateBeerInfo");
      })
  };

  onChange = e => {
    this.setState({ [e.target.name]: e.target.value });
  };

  onSubmit = e => {
    e.preventDefault();

    const data = {
      name: this.state.name,
      brewer: this.state.brewer,
      description: this.state.description,
      tasted_date: this.state.tasted_date,
      notes: this.state.notes
    };

    axios
      .put('http://localhost:8082/beers/'+this.props.match.params.id, data)
      .then(res => {
        this.props.history.push('/show-beer/'+this.props.match.params.id);
      })
      .catch(err => {
        console.log("Error in UpdateBeerInfo!");
      })
  };


  render() {
    return (
      <div className="UpdateBeerInfo">
        <div className="container">
          <div className="row">
            <div className="col-md-8 m-auto">
              <br />
              <Link to="/" className="btn btn-outline-info float-left">
                  Show Beer List
              </Link>
            </div>
            <div className="col-md-8 m-auto">
              <h1 className="display-4 text-center">Edit Beer</h1>
              <p className="lead text-center">
                  Update Beer's Info
              </p>
            </div>
          </div>

          <div className="col-md-8 m-auto">
          <form noValidate onSubmit={this.onSubmit}>
            <div className='form-group'>
              <label htmlFor="name">Name</label>
              <input
                type='text'
                placeholder='Name of the Beer'
                name='name'
                className='form-control'
                value={this.state.name}
                onChange={this.onChange}
              />
            </div>
            <br />

            <div className='form-group'>
            <label htmlFor="brewer">Brewer</label>
              <input
                type='text'
                placeholder='Brewer'
                name='brewer'
                className='form-control'
                value={this.state.brewer}
                onChange={this.onChange}
              />
            </div>

            <div className='form-group'>
            <label htmlFor="description">Description</label>
              <input
                type='text'
                placeholder='Describe this beer'
                name='description'
                className='form-control'
                value={this.state.description}
                onChange={this.onChange}
              />
            </div>

            <div className='form-group'>
            <label htmlFor="published_date">Tasted Date</label>
              <input
                type='date'
                placeholder='tasted_date'
                name='tasted_date'
                className='form-control'
                value={this.state.tasted_date}
                onChange={this.onChange}
              />
            </div>
            <div className='form-group'>
            <label htmlFor="publisher">Notes</label>
              <input
                type='text'
                placeholder='Notes of this Beer'
                name='publnotesisher'
                className='form-control'
                value={this.state.notes}
                onChange={this.onChange}
              />
            </div>

            <button type="submit" className="btn btn-outline-secondary btn-lg btn-block">Update Beer</button>
            </form>
          </div>

        </div>
      </div>
    );
  }
}

export default UpdateBeerInfo;