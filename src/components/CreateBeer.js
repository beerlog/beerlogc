import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import '../App.css';
import axios from 'axios';


class CreateBeer extends Component {
  constructor() {
    super();
    this.state = {
      name: '',
      brewer:'',
      description:'',
      tasted_date:'',
      notes:''
    };
  }

  onChange = e => {
    this.setState({ [e.target.name]: e.target.value });
  };

  onSubmit = e => {
    e.preventDefault();

    const data = {
      name: this.state.name,
      brewer: this.state.brewer,
      description: this.state.description,
      tasted_date: this.state.tasted_date,
      notes: this.state.notes
    };

    axios
      .post('http://localhost:8082/beers', data)
      .then(res => {
        this.setState({
          name: '',
          brewer:'',
          description:'',
          tasted_date:'',
          notes:''
        })
        this.props.history.push('/');
      })
      .catch(err => {
        console.log("Error in CreateBeer!");
      })
  };

  render() {
    return (
      <div className="CreateBeer">
        <div className="container">
          <div className="row">
            <div className="col-md-8 m-auto">
              <br />
              <Link to="/" className="btn btn-outline-info float-left">
                  Show Beer List
              </Link>
            </div>
            <div className="col-md-8 m-auto">
              <h1 className="display-4 text-center">Add Beer</h1>
              <p className="lead text-center">
                  Create new beer
              </p>

              <form noValidate onSubmit={this.onSubmit}>
                <div className='form-group'>
                  <input
                    type='text'
                    placeholder='Name of the Beer'
                    name='name'
                    className='form-control'
                    value={this.state.name}
                    onChange={this.onChange}
                  />
                </div>
                <br />

                <div className='form-group'>
                  <input
                    type='text'
                    placeholder='Brewer'
                    name='brewer'
                    className='form-control'
                    value={this.state.brewer}
                    onChange={this.onChange}
                  />
                </div>

                <div className='form-group'>
                  <input
                    type='text'
                    placeholder='Describe this beer'
                    name='description'
                    className='form-control'
                    value={this.state.description}
                    onChange={this.onChange}
                  />
                </div>

                <div className='form-group'>
                  <input
                    type='date'
                    placeholder='tasted_date'
                    name='tasted_date'
                    className='form-control'
                    value={this.state.tasted_date}
                    onChange={this.onChange}
                  />
                </div>
                <div className='form-group'>
                  <input
                    type='text'
                    placeholder='Notes on this Beer'
                    name='notes'
                    className='form-control'
                    value={this.state.notes}
                    onChange={this.onChange}
                  />
                </div>

                <input
                    type="submit"
                    className="btn btn-outline-secondary btn-block mt-4"
                />
              </form>
          </div>
          </div>
        </div>
      </div>
    );
  }
}

export default CreateBeer;