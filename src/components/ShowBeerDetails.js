import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import '../App.css';
import axios from 'axios';

class showBeerDetails extends Component {
  constructor(props) {
    super(props);
    this.state = {
      beer: {}
    };
  }

  componentDidMount() {
    // console.log("Print id: " + this.props.match.params.id);
    axios
      .get('http://localhost:8082/beers/'+this.props.match.params.id)
      .then(res => {
        // console.log("Print-showBeerDetails-API-response: " + res.data);
        this.setState({
          beer: res.data
        })
      })
      .catch(err => {
        console.log("Error from ShowBeerDetails");
      })
  };

  onDeleteClick (id) {
    axios
      .delete('http://localhost:8082/beers/'+id)
      .then(res => {
        this.props.history.push("/");
      })
      .catch(err => {
        console.log("Error form ShowBeerDetails_deleteClick");
      })
  };


  render() {

    const beer = this.state.beer;
    let BeerItem = <div>
      <table className="table table-striped table-light">
        {/* <thead>
          <tr>
            <th scope="col">#</th>
            <th scope="col">First</th>
            <th scope="col">Last</th>
            <th scope="col">Handle</th>
          </tr>
        </thead> */}
        <tbody>
          <tr>
            <th scope="row">1</th>
            <td>Name</td>
            <td>{ beer.name }</td>
          </tr>
          <tr>
            <th scope="row">2</th>
            <td>Brewer</td>
            <td>{ beer.brewer }</td>
          </tr>
          <tr>
            <th scope="row">6</th>
            <td>Description</td>
            <td>{ beer.description }</td>
          </tr>
          <tr>
            <th scope="row">5</th>
            <td>Tasted Date</td>
            <td>{ beer.tasted_date }</td>
          </tr>
          <tr>
            <th scope="row">6</th>
            <td>Notes</td>
            <td>{ beer.notes }</td>
          </tr>
        </tbody>
      </table>
    </div>

    return (
      <div className="ShowBeerDetails">
        <div className="container">
          <div className="row">
            <div className="col-md-10 m-auto">
              <br /> <br />
              <Link to="/" className="btn btn-outline-info float-left">
                  Show Beer List
              </Link>
            </div>
            <br />
            <div className="col-md-8 m-auto">
              <h1 className="display-4 text-center">Beer's Record</h1>
              <p className="lead text-center">
                  View Beer's Info
              </p>
              <hr /> <br />
            </div>
          </div>
          <div className="BeerInfo">
            { BeerItem }
          </div>

          <div className="row">
            <div className="col-md-6">
              <button type="button" className="btn btn-outline-danger btn-lg btn-block" onClick={this.onDeleteClick.bind(this,beer._id)}>Delete Beer</button><br />
            </div>

            <div className="col-md-6">
              <Link to={`/edit-beer/${beer._id}`} className="btn btn-outline-secondary btn-lg btn-block">
                    Edit Beer
              </Link>
              <br />
            </div>

          </div>
            {/* <br />
            <button type="button" class="btn btn-outline-info btn-lg btn-block">Edit Beer</button>
            <button type="button" class="btn btn-outline-danger btn-lg btn-block">Delete Beer</button> */}

        </div>
      </div>
    );
  }
}

export default showBeerDetails;