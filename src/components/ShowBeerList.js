import React, { Component } from 'react';
import '../App.css';
import axios from 'axios';
import { Link } from 'react-router-dom';
import BeerCard from './BeerCard';

class ShowBeerList extends Component {
  constructor(props) {
    super(props);
    this.state = {
      beers: []
    };
  }

  componentDidMount() {
    axios
      .get('http://localhost:8082/beers')
      .then(res => {
        this.setState({
          beers: res.data
        })
      })
      .catch(err =>{
        console.log('Error from ShowBeerList');
      })
  };


  render() {
    const beers = this.state.beers;
    console.log("Print Beer: " + beers);
    let beerList;

    if(!beers) {
      beerList = "there is no beer record!";
    } else {
      beerList = beers.map((beer, k) =>
        <BeerCard beer={beer} key={k} />
      );
    }

    return (
      <div className="ShowBeerList">
        <div className="container">
          <div className="row">
            <div className="col-md-12">
              <br />
              <h2 className="display-4 text-center">Beers List</h2>
            </div>

            <div className="col-md-11">
              <Link to="/create-beer" className="btn btn-outline-info float-right">
                + Add New Beer
              </Link>
              <br />
              <br />
              <hr />
            </div>

          </div>

          <div className="list">
                {beerList}
          </div>
        </div>
      </div>
    );
  }
}

export default ShowBeerList;